use std::io;
use rand::{thread_rng, Rng};

pub fn run() {
    let mut count: i32 = 6;
    let mut _x: i32;
    println!("Hello one and all! Guess the number");
    let mut line = String::new();
    //https://docs.rs/rand/0.8.5/rand/trait.Rng.html
    let sec_num = thread_rng()
        .gen_range(1..=100);
    loop {
        count -= 1;
        println!("Give number:");
        io::stdin()
            .read_line(&mut line)
            .expect("Failed to read line");
        _x = line
            .trim()
            .parse::<i32>()
            .expect("Input not an integer");
        line.clear();
            if _x == sec_num {
                println!("Bingo! You wone!");
                break;
            } else if _x < sec_num {
                println!("Take bigger, {} attempts left", count);
            } else {
                println!("Take smaller, {} attempts left", count);
            }
        if count == 0 {
            println!("You lose, game over");
            break;
        }
    }
    println!("Secret number: {}", sec_num);
    println!("Attempts left: {}", count);
}

//fn type_of<T>(_: &T) -> &'static str {
//    std::any::type_name::<T>()
//}
